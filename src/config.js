// export const BASE_URL = "http://3.17.29.219:8000";
// export const INTERMEDIATE_SERVER_URL = "http://3.17.29.219:4500";
// export const SOCKET_SERVER = "http://3.17.29.219:5000";

export const BASE_URL = "http://localhost:8000";
export const INTERMEDIATE_SERVER_URL = "http://localhost:4500";
export const SOCKET_SERVER = "http://localhost:5000";
