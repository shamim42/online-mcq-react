export const MAKE_LOG_IN = "MAKE_LOG_IN";
export const MAKE_LOG_OUT = "MAKE_LOG_OUT";
export const KEEP_EXAM_LIST = "KEEP_EXAM_LIST";
export const EXAM_LOADING = "EXAM_LOADING";
export const QUESTION_LOADING = "QUESTION_LOADING";
export const KEEP_QUESTION = "KEEP_QUESTION";
export const EXAM_TIME_EXPIRED = "EXAM_TIME_EXPIRED";
export const KEEP_EXAM_RESULT = "KEEP_EXAM_RESULT";
export const MAKE_EXAM_DONE = "MAKE_EXAM_DONE";
export const BEGIN_EXAM_STARTED = "BEGIN_EXAM_STARTED";
export const BEGIN_EXAM_END = "BEGIN_EXAM_END";
export const KEEP_TIME_DURATION = "BEGIN_EXAM_END";
export const IS_EXAM_DONE = "IS_EXAM_DONE";
export const KEEP_TOTAL_MARKS = "KEEP_TOTAL_MARKS";
export const CHANGE_USER_STATUS_IN_REDUX = "CHANGE_USER_STATUS_IN_REDUX";
export const EVERYTHING_OK_TO_START_THE_EXAM = "EVERYTHING_OK_TO_START_THE_EXAM";

export const USERS_EXAM_LIST_FETCHING_STARTED = "EVERYTHING_OK_TO_START_THE_EXAM";
export const USERS_EXAM_LIST_FETCHING_SUCCESS = "USER_EXAM_LIST_FETCHING_SUCCESS";
export const USERS_EXAM_LIST_FETCHING_FAILED = "USER_EXAM_LIST_FETCHING_FAILED";


export const FETCHING_ALL_USERS_STARTED = "FETCHING_ALL_USERS_STARTED";
export const FETCHING_ALL_USERS_DONE_WITH_SUCCESS = "FETCHING_ALL_USERS_DONE_WITH_SUCCESS";
export const FETCHING_ALL_USERS_DONE_WITH_FAILED = "FETCHING_ALL_USERS_DONE_WITH_FAILED";

