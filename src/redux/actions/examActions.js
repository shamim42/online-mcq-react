import Axios from 'axios';
import {
    KEEP_EXAM_LIST,
    EXAM_LOADING,
    QUESTION_LOADING,
    KEEP_QUESTION,
    EXAM_TIME_EXPIRED,
    KEEP_EXAM_RESULT,
    KEEP_TOTAL_MARKS,
    KEEP_TIME_DURATION,
    IS_EXAM_DONE,
    MAKE_EXAM_DONE,
    BEGIN_EXAM_STARTED,
    BEGIN_EXAM_END,
    EVERYTHING_OK_TO_START_THE_EXAM,

} from './types';

import { BASE_URL, INTERMEDIATE_SERVER_URL } from '../../config'

export const start_fetching_exams = () => {
    return {
        type: EXAM_LOADING,
        payload: {
            exam_loading: 1,
        },
    };
};

export const end_fetching_exams_with_success = () => {
    return {
        type: EXAM_LOADING,
        payload: {
            exam_loading: 0,
        },
    };
};

export const end_fetching_exams_with_failure = () => {
    return {
        type: EXAM_LOADING,
        payload: {
            exam_loading: -1,
        },
    };
};

export const keep_exam_list = (exams) => {
    return {
        type: KEEP_EXAM_LIST,
        payload: {
            exams: exams,
        },
    };
};


export const exam_time_expired = (flag) => {
    return {
        type: EXAM_TIME_EXPIRED,
        payload: {
            flag: flag,
        },
    };
};




export const start_fetching_question = () => {
    return {
        type: QUESTION_LOADING,
        payload: {
            question_loading: 1,
        },
    };
};

export const end_fetching_question_with_success = () => {
    return {
        type: QUESTION_LOADING,
        payload: {
            question_loading: 0,
        },
    };
};

export const end_fetching_question_with_failure = () => {
    return {
        type: QUESTION_LOADING,
        payload: {
            question_loading: -1,
        },
    };
};



export const keep_question = (question) => {
    return {
        type: KEEP_QUESTION,
        payload: {
            question: question,
        },
    };
};


export const keep_correct_answer = (question) => {
    return {
        type: KEEP_QUESTION,
        payload: {
            question: question,
        },
    };
};


export const keep_exam_result = (exam_result) => {
    return {
        type: KEEP_EXAM_RESULT,
        payload: {
            exam_result: exam_result,
        },
    };
};


export const keep_time_duration = (time) => {
    console.log('inside keeptimeduration action')
    return {
        type: KEEP_TIME_DURATION,
        payload: time,
    };
};


export const keep_total_marks = (marks) => {
    return {
        type: KEEP_TOTAL_MARKS,
        payload: marks,
    };
};


export const is_exam_done = (flag) => {
    return {
        type: IS_EXAM_DONE,
        payload: flag,
    };
};




export const save_answer = (data, jwt_token) => {
    console.log('inside exam action')
    return (
        dispatch => {
            Axios({
                method: "POST",
                url: INTERMEDIATE_SERVER_URL + "/save-answer/" + data.exam_key,
                data: {
                    questionId: data.questionId,
                    selected: data.selected
                },
                headers: {
                    "Authorization": jwt_token
                }
            }).then(res => {
                if (res.data.message == 'exam_result') {
                    console.log('exam result is');
                    console.log(res.data.data)
                    // dispatch(make_exam_done());
                    dispatch(keep_exam_result(res.data.data))
                } else {
                    console.log(res.data)
                    dispatch(keep_question(res.data.data))
                }
            }).catch(err => {
                console.log('result making problem or saving problem')
            })
        }
    )
}



export const fetch_all_exams = (jwt_token) => {
    return ((dispatch) => {
        dispatch(start_fetching_exams());

        Axios({
            method: "GET",
            url: BASE_URL + "/api/mcq/exam/",
            headers: {
                "Authorization": jwt_token
            }
        }).then(res => {
            dispatch(keep_exam_list(res.data.data));
            dispatch(end_fetching_exams_with_success());
        })
            .catch(err => {
                dispatch(end_fetching_exams_with_failure());
                console.log('error to fetch examlist')

            })
    }
    )
};


export const fetch_a_question = (exam_key, jwt_token) => {
    return ((dispatch) => {
        dispatch(start_fetching_question());

        Axios({
            method: "GET",
            url: INTERMEDIATE_SERVER_URL + "/exam-running/" + exam_key,
            headers: {
                "Authorization": jwt_token
            }
        }).then(res => {
            console.log(res.data)
            if (res.data.status == false && res.data.message == 'expired') {
                dispatch(exam_time_expired(1))
            }
            else {
                dispatch(keep_question(res.data.data));
                dispatch(end_fetching_question_with_success());
            }

        })
            .catch(err => {
                dispatch(end_fetching_question_with_failure());
            })
    }
    )
};


