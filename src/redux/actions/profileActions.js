import Axios from "axios"
import { BASE_URL } from "../../config";
import {
    USERS_EXAM_LIST_FETCHING_STARTED,
    USERS_EXAM_LIST_FETCHING_SUCCESS,
    USERS_EXAM_LIST_FETCHING_FAILED,
    FETCHING_ALL_USERS_STARTED,
    FETCHING_ALL_USERS_DONE,
    FETCHING_ALL_USERS_DONE_WITH_SUCCESS,
    FETCHING_ALL_USERS_DONE_WITH_FAILED,
    CHANGE_USER_STATUS_IN_REDUX,
} from './types'

const users_exam_list_fetching_started = () => {
    return {
        type: USERS_EXAM_LIST_FETCHING_STARTED,
    }
}

const users_exam_list_fetching_success = (response) => {
    return {
        type: USERS_EXAM_LIST_FETCHING_SUCCESS,
        payload: response
    }
}

const users_exam_list_fetching_failed = (error) => {
    return {
        type: USERS_EXAM_LIST_FETCHING_FAILED,
        payload: error
    }
}


const fetching_all_users_started = () => {
    return {
        type: FETCHING_ALL_USERS_STARTED,
    }
}

const fetching_all_users_done_with_success = (data) => {
    return {
        type: FETCHING_ALL_USERS_DONE_WITH_SUCCESS,
        payload: data
    }
}

const fetching_all_users_done_with_failed = (err) => {
    return {
        type: FETCHING_ALL_USERS_DONE_WITH_FAILED,
        payload: err
    }
}

export const change_user_status_in_redux = (user_id, superuser_flag) => {
    return {
        type: CHANGE_USER_STATUS_IN_REDUX,
        payload: {
            user_id: user_id,
            superuser_flag: superuser_flag,
        }
    }
}




export const fetch_users_exam_list = (jwt_token) => {

    return dispatch => {
        dispatch(users_exam_list_fetching_started());
        Axios({
            method: "GET",
            url: BASE_URL + "/api/mcq/user-exam/",
            headers: {
                "Authorization": jwt_token
            }
        }).then(response => {
            console.log(response.data)
            dispatch(users_exam_list_fetching_success(response.data))
        }).catch(err => {
            console.log(err)
            dispatch(users_exam_list_fetching_failed('error to fetch users-exams map'))
        })
    }
}

export const fetch_all_users = (jwt_token) => {
    console.log('starting')
    return dispatch => {
        dispatch(fetching_all_users_started());
        Axios({
            method: "GET",
            url: BASE_URL + "/api/mcq/user/",
            headers: {
                "Authorization": jwt_token
            }
        }).then(response => {
            console.log(response.data)
            dispatch(fetching_all_users_done_with_success(response.data))
        }).catch(err => {
            console.log(err)
            dispatch(fetching_all_users_done_with_failed(err))
        })
    }
}

export const make_superuser = (user_id, jwt_token) => {
    return dispatch => {
        Axios({
            method: "POST",
            url: BASE_URL + "/api/make-superuser/",
            headers: {
                "Authorization": jwt_token
            },
            data: {
                "proposedUserId": user_id,
            }
        }).then(response => {
            dispatch(change_user_status_in_redux(user_id, 1));
        }).catch(err => {
            // dispatch(fetching_all_users_done_with_failed(err))
        })
    }
}


export const remove_superuser = (user_id, jwt_token) => {
    return dispatch => {
        // dispatch(fetching_all_users_started());
        Axios({
            method: "POST",
            url: BASE_URL + "/api/remove-superuser/",
            headers: {
                "Authorization": jwt_token
            },
            data: {
                "proposedUserId": user_id,
            }
        }).then(response => {
            dispatch(change_user_status_in_redux(user_id, 0));
            // dispatch(fetching_all_users_done_with_success(response.data))
        }).catch(err => {
            // dispatch(fetching_all_users_done_with_failed(err))
        })
    }
}



