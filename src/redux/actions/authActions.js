import {
  MAKE_LOG_IN,
  MAKE_LOG_OUT,
} from './types';

// auth actions
export const make_log_in = (name, user_id, mobile, jwt_token) => {
  return {
    type: MAKE_LOG_IN,
    payload: {
      name: name,
      user_id: user_id,
      mobile: mobile,
      jwt_token: jwt_token
    },
  };
};

export const make_log_out = () => {
  return {
    type: MAKE_LOG_OUT,
  };
};
