import {
  MAKE_LOG_IN, MAKE_LOG_OUT
} from '../actions/types';

// initial state
let initialState = {
  is_logged_in: false,
  jwt_token: '',
  user_id: '',
  user_name: '',
  user_mobile: "",
  jwt_token: "",

};

// auth reducer switch case
const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case MAKE_LOG_IN:
      console.log(action.payload)
      state.is_logged_in = true;
      state.user_id = action.payload.user_id;
      state.user_name = action.payload.name;
      state.user_mobile = action.payload.mobile;
      state.jwt_token = action.payload.jwt_token;
      console.log(action.payload.jwt_token)
      return Object.assign({}, state);

    case MAKE_LOG_OUT:
      state.is_logged_in = false;
      state.jwt_token = "";
      state.user_id = "";
      state.user_name = "";
      state.user_mobile = "";
      state.jwt_token = "";
      return Object.assign({}, state);


    default:
      return state;
  }
};

export default authReducer;
