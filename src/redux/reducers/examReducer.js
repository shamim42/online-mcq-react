import { act } from '@testing-library/react';
import {
  KEEP_EXAM_LIST,
  EXAM_LOADING,
  QUESTION_LOADING,
  KEEP_QUESTION,
  EXAM_TIME_EXPIRED,
  KEEP_EXAM_RESULT,
  KEEP_TIME_DURATION,
  KEEP_TOTAL_MARKS,
  IS_EXAM_DONE,
  // BEGIN_EXAM_END,
  // BEGIN_EXAM_STARTED,
  // EVERYTHING_OK_TO_START_THE_EXAM, KEEP_TIME_DURATION, KEEP_TOTAL_MARKS
} from '../actions/types';

// initial state
let initialState = {
  allExams: [],
  exam_loading: 0,

  exam_expired: 0,

  question_loading: 0,
  question_info: {},

  exam_key: "",
  exam_result: "",
  exam_done: false,

  time_duration: "",
  total_marks: "",

  creating_exam: 0,
  exam_started: 0,
  exam_already_given: 0,
  everything_ok_to_start_the_exam: 0,
};

// auth reducer switch case
const examReducer = (state = initialState, action) => {
  switch (action.type) {
    case KEEP_EXAM_LIST:
      console.log(action.payload.exams)
      state.allExams = action.payload.exams;
      return Object.assign({}, state);

    case EXAM_TIME_EXPIRED:
      state.exam_expired = action.payload.flag;
      return Object.assign({}, state);



    case EXAM_LOADING:
      state.exam_loading = action.payload.exam_loading;
      return Object.assign({}, state);

    case IS_EXAM_DONE:
      state.exam_done = action.payload;
      return Object.assign({}, state);


    case QUESTION_LOADING:
      state.question_loading = action.payload.question_loading;
      return Object.assign({}, state);

    case KEEP_QUESTION:
      state.question_info = action.payload.question;
      return Object.assign({}, state);

    case KEEP_EXAM_RESULT:
      state.exam_done = true;
      state.exam_result = action.payload.exam_result;
      return Object.assign({}, state);

    case KEEP_TIME_DURATION:
      state.time_duration = action.payload;
      console.log(action.payload)
      return Object.assign({}, state);

    case KEEP_TOTAL_MARKS:
      state.total_marks = action.payload;
      return Object.assign({}, state);



    // case BEGIN_EXAM_STARTED:
    //   state.exam_started = 1;
    //   state.creating_exam = 1;
    //   return Object.assign({}, state)


    // case BEGIN_EXAM_END:
    //   state.exam_started = 0;
    //   state.creating_exam = 0;
    //   if (action.payload.response.data.status == false && action.payload.response.data.message == "already_given") {
    //     state.exam_already_given = 1;
    //   }

    //   if (action.payload.response.data.status == true && action.payload.response.data.data.hasOwnProperty('exam_key')) {
    //     state.exam_key = action.payload.response.data.data.key;
    //   }

    //   return Object.assign({}, state)

    // case EVERYTHING_OK_TO_START_THE_EXAM:
    //   state.everything_ok_to_start_the_exam = action.payload
    //   return Object.assign({}, state)


    default:
      return state;
  }
};

export default examReducer;
