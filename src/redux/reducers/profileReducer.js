import {
    USERS_EXAM_LIST_FETCHING_SUCCESS,
    USERS_EXAM_LIST_FETCHING_STARTED,
    USERS_EXAM_LIST_FETCHING_FAILED,
    FETCHING_ALL_USERS_STARTED,
    FETCHING_ALL_USERS_DONE,
    FETCHING_ALL_USERS_DONE_WITH_SUCCESS,
    FETCHING_ALL_USERS_DONE_WITH_FAILED,
    CHANGE_USER_STATUS_IN_REDUX,
} from '../actions/types'

let user_id;
let superuser_flag;


let initialState = {
    users_exam_loading: false,
    users_exams: [],
    users_exam_error: "",


    all_users: [], //super users need to see all users
    fetching_all_users: 0,
    error_in_fetching_all_users: "",
}

const profileReducer = (state = initialState, actions) => {
    switch (actions.type) {
        case USERS_EXAM_LIST_FETCHING_STARTED:
            state.users_exam_loading = true;
            return Object.assign({}, state);

        case USERS_EXAM_LIST_FETCHING_SUCCESS:
            state.users_exam_loading = false;
            state.users_exams = actions.payload['data'];
            return Object.assign({}, state);

        case USERS_EXAM_LIST_FETCHING_FAILED:
            state.users_exam_loading = false;
            state.users_exam_error = actions.payload;
            return Object.assign({}, state);


        case FETCHING_ALL_USERS_STARTED:
            state.fetching_all_users = 1;
            return Object.assign({}, state);

        case FETCHING_ALL_USERS_DONE_WITH_SUCCESS:
            console.log(actions.payload.data)
            state.fetching_all_users = 0;
            state.error_in_fetching_all_users = "";
            state.all_users = actions.payload.data;
            return Object.assign({}, state)

        case FETCHING_ALL_USERS_DONE_WITH_FAILED:
            state.error_in_fetching_all_users = actions.payload;
            state.all_users = [];
            return Object.assign({}, state);

        case CHANGE_USER_STATUS_IN_REDUX:
            user_id = actions.payload.user_id;
            superuser_flag = actions.payload.superuser_flag;

            state.all_users.some((user, index) => {
                if (user.id == user_id) {
                    state.all_users[index]['is_superuser'] = superuser_flag;
                    return;
                }
            })
            return Object.assign({}, state);



        default:
            return state
    }


}
export default profileReducer;































































































