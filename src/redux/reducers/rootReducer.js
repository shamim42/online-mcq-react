import {
  combineReducers
} from 'redux';

import authReducer from './authReducer';
import examReducer from './examReducer';
import profileReducer from './profileReducer';


const rootReducer = combineReducers({
  auth: authReducer,
  exams: examReducer,
  profile: profileReducer,
})
export default rootReducer;