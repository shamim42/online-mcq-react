import { createStore, compose, applyMiddleware } from 'redux';
import rootReducer from './reducers/rootReducer';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import ReduxThunk from 'redux-thunk';
import examReducer from './reducers/examReducer';


const persistConfig = {
    key: 'root',
    storage,
    stateReconciler: autoMergeLevel2,
    whitelist: ['exams.allExams', 'auth', 'profile.users_exam'],
    // blacklist: ['exams.everything_ok_to_start_the_exam'],
}
const persistedReducer = persistReducer(persistConfig, rootReducer)

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
// export let store = createStore(persistedReducer, composeEnhancers(applyMiddleware(ReduxThunk)));
// export let persistor = persistStore(store);


export let store = createStore(persistedReducer, composeEnhancers(
    applyMiddleware(ReduxThunk),
)
);
export let persistor = persistStore(store);