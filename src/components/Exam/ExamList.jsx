import React, { Component, } from 'react';
import { Row, Col, Table, Button, Modal, Spin } from 'antd';
import { fetch_all_exams, is_exam_done, keep_time_duration, keep_total_marks } from '../../redux/actions/examActions';
import { connect } from 'react-redux';
import Axios from 'axios';
import { INTERMEDIATE_SERVER_URL } from '../../config'
import { fetch_users_exam_list } from '../../redux/actions/profileActions';
import { Redirect } from 'react-router-dom';

class ExamList extends Component {
    state = {
        visible: false,
        creating_exam: false,
        clicked_exam_id: "",
        already_given_exam: 0,
        no_question_available: 0,
    };

    componentDidMount() {
        this.props.fetchAllExam(this.props.auth.jwt_token);
        this.props.fetchAllGivenExam(this.props.auth.user_id, this.props.auth.jwt_token);
    }


    showModal = (exam_id) => {
        this.setState({
            visible: true,
            clicked_exam_id: exam_id,
            creating_exam: false,
            already_given_exam: 0,
            no_question_available: 0,
        });
    };

    handleOk = e => {

        this.setState({
            creating_exam: 1,
        });


        this.beginExam(this.props.auth.user_id, this.state.clicked_exam_id)
            .then(response => {
                console.log(response.data.data.key)

                if (response.data.status == false && response.data.message == 'already_given') {
                    this.setState({
                        already_given_exam: 1,
                        creating_exam: 0,
                    });
                }
                else if (response.data.data.key) {
                    // let exam_key = response.data.key;
                    let exam_key = response.data.data.key;
                    console.log('inside response.data.key ')
                    // let time_duration = response.data.data.duration;
                    // let total_marks = response.data.data.total_marks;
                    let time_duration = 5;
                    let total_marks = 5;


                    this.props.keepTimeDuration(time_duration);
                    this.props.keepTotalMarks(total_marks);

                    //fetch the first question
                    this.getFirstQuestion(exam_key, this.props.auth.jwt_token)
                        .then(res => {
                            console.log('first question arrived in react examlist component')
                            console.log(res.data)
                            if (res.data.data) {
                                this.setState({
                                    creating_exam: 0,
                                })
                                this.props.isExamDone(false); //initially we are setting this flag false. when user will complete the exam this will be true
                                this.props.history.push("exam/" + exam_key);
                            }
                            else {
                                this.setState({
                                    no_question_available: 1,
                                    creating_exam: 0,
                                })
                            }

                        })
                        .catch(err => {
                            console.log('first question can not be achieved')
                            console.log(err)
                        })

                }
            })
            .catch(err => {
                this.setState({
                    creating_exam: -1
                })
            })
    };


    beginExam = (userId, examId) => {
        return new Promise((resolve, reject) => {
            Axios({
                method: "POST",
                url: INTERMEDIATE_SERVER_URL + "/begin-exam",
                data: {
                    "userId": userId,
                    "examId": examId
                },
                headers: {
                    "Authorization": this.props.auth.jwt_token,
                }
            })
                .then(response => {
                    console.log('exam started')
                    resolve(response);

                })
                .catch(err => {
                    console.log('something went error to create the exam paper');
                    reject('failed')
                })
        })
    }

    getFirstQuestion = (examKey, jwt_token) => {
        console.log('get first question started')
        return new Promise((resolve, reject) => {
            Axios({
                method: "GET",
                url: INTERMEDIATE_SERVER_URL + "/get-first-question/" + examKey,
                headers: {
                    "Authorization": jwt_token
                }
            })
                .then(response => {
                    console.log('get first queston success');
                    resolve(response);
                })
                .catch(err => {
                    console.log('get first question ended with error');
                    reject('failed')
                })
        })
    }

    handleCancel = e => {
        console.log(e);
        this.setState({
            visible: false,
            creating_exam: false,
        });
    };

    redirectToProfile = () => {
        this.props.history.push('/profile');
    }


    render() {
        // if is_logged_in === false redirect to login page
        if (!this.props.auth.is_logged_in)
            return <Redirect to="/signin" />;

        const all_exams = this.props.exams.allExams;
        let user_given_all_exams = this.props.profile.users_exams.map(el => el.fk_exam_id);
        user_given_all_exams = Array.from(user_given_all_exams);

        const dataHouse = all_exams.map(exam => {
            return ({
                name: exam.title,
                total_marks: exam.total_marks,
                duration: exam.duration,
                exam_id: exam.id,
            })
        });
        // const dataSource = [
        //     {
        //         key: '1',
        //         name: 'Mike',
        //         age: 32,
        //         address: 'Start'
        //     },
        //     {
        //         key: '2',
        //         name: 'John',
        //         age: 42,
        //         address: 'Start',
        //     },
        //     {
        //         key: '2',
        //         name: 'John',
        //         age: 42,
        //         address: 'Start',
        //     },
        // ];

        const columns = [
            {
                title: 'Name',
                dataIndex: 'name',
                key: 0,
                width: '40%'
            },
            {
                title: 'Total Marks',
                dataIndex: 'total_marks',
                key: 1,
            },
            {
                title: 'Time Duration',
                dataIndex: 'duration',
                key: 2,
            },
            {
                title: 'Action',
                dataIndex: 'exam_id',
                key: 3,
                render: (id) => {
                    let is_exam_already_given = user_given_all_exams.includes(id) ? true : false
                    return (
                        <Button type="primary" disabled={is_exam_already_given} onClick={this.showModal.bind(this, id)}>{is_exam_already_given ? "Given" : "Start"}</Button>
                    )
                },
            },
        ];
        return (
            <div>
                <Row justify="center">
                    <Col>
                        <h1 className="headerText">BYSL Online MCQ Exam</h1>
                    </Col>
                </Row>
                <Row justify="center">
                    <Col md={18} lg={12}>
                        <div className="custom-layout">
                            <Row justify="space-between">
                                <h3>Start an exam that fits you.</h3>
                                <Button type="primary" onClick={this.redirectToProfile}>My Profile</Button>
                            </Row>
                            <br />
                            <Table dataSource={dataHouse} columns={columns} />
                        </div>
                    </Col>
                </Row>
                <Modal
                    title={this.state.creating_exam ? "" : "Warning"}
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    okText="Start"
                >
                    {
                        this.state.already_given_exam == 1 ? (
                            <h3 className="creatingExamWrongMessageContainer">This exam has already been given !</h3>
                        ) : (
                                this.state.creating_exam == 1 ? (
                                    <>
                                        <div className="creatingExamSpinnerContainer">
                                            <Spin large tip="Creating your exam paper..." />
                                        </div>
                                    </>
                                ) :
                                    this.state.creating_exam == 0 ? (
                                            this.state.no_question_available == 1 ?
                                                (
                                                    <h3 className="creatingExamWrongMessageContainer">No question available for this exam !</h3>
                                                )
                                                :
                                                (
                                                    <h3>Once you start the exam, can't pause it. When you will end the exam explicitly or be ended by any connectivity error the result will be saved in your profile.</h3>
                                                )
                                    ) : (
                                            <h3 className="creatingExamWrongMessageContainer">Something went wrong to create the exam paper !</h3>
                                        )
                            )

                    }
                    {

                    }
                </Modal>
            </div >
        )
    }
}
const mapStateToProps = (state) => ({
    exams: state.exams,
    auth: state.auth,
    profile: state.profile,
})

const mapDispatchToProps = (dispatch) => ({
    fetchAllExam: (jwt_token) => {
        dispatch(fetch_all_exams(jwt_token));
    },
    fetchAllGivenExam: (userId, jwt_token) => {
        dispatch(fetch_users_exam_list(userId, jwt_token));
    },
    keepTimeDuration: (time) => {
        console.log('keep time duration in exam list')
        dispatch(keep_time_duration(time))
    },
    keepTotalMarks: (time) => {
        dispatch(keep_total_marks(time))
    },
    isExamDone: (flag) => {
        dispatch(is_exam_done(flag))
    },


})
export default connect(mapStateToProps, mapDispatchToProps)(ExamList);