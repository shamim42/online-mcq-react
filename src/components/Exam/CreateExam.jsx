import { Row, Col, Input, Button } from 'antd'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export default class CreateExam extends Component {
    render() {
        return (
            <div>
                <Row justify="center">
                    <Col>
                        <h1 className="headerText">BYSL Online MCQ Exam</h1>
                    </Col>
                </Row>
                <Row justify="center">
                    <Col md={18} lg={12}>
                        <div className="custom-layout">
                            <Row justify="space-between">
                                <Col><h2>Create Exam Paper</h2></Col>
                                <Col><Button type="primary"><Link to="/profile">My Profile</Link></Button></Col>
                            </Row>
                            {/* <Row>
                                <Col span={24}>
                                    <lable>Title:</lable>
                                    <Input size="large" placeholder="Exam Title" />
                                </Col>
                            </Row> */}
                            <div style={{ padding: '3rem', textAlign: "center", backgroundColor: "#fef" }}>
                                <h1>This module is under development</h1>

                            </div>
                        </div>
                    </Col>
                </Row >
            </div >
        )
    }
}
