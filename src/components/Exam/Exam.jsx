import React, { Component } from 'react';
import io from 'socket.io-client';
import { Redirect } from 'react-router-dom'
import { Row, Col, Radio, Button, Spin } from 'antd';
import { exam_time_expired, fetch_a_question, keep_exam_result, save_answer } from '../../redux/actions/examActions';
import { connect } from 'react-redux';
import { SOCKET_SERVER } from '../../config'

class Exam extends Component {
    state = {
        selected_option: '',
        passed_minutes: 0,
        passed_seconds: 0,
        exam_done: false,
    }

    componentDidMount() {
        this.props.exam_expired(0); //initially exam is not expired
        let exam_key = this.props.match.params.exam_key;
        this.props.fetch_question(exam_key, this.props.auth.jwt_token);

        // const socket = io('localhost:5000');
        const socket = io(SOCKET_SERVER);

        socket.on('connect', () => {
            console.log('socket connected')
        });

        socket.on(exam_key, (data) => {
            if (data.hasOwnProperty('exam_done')) {
                // this.props.keepExamResult(data.exam_result);
                this.props.exam_expired(1);
            }

            let time = data;
            this.setState({
                passed_minutes: time.minutes,
                passed_seconds: time.seconds,
            })
        })
    }


    fetchNextQuestion = () => {
        this.setState({
            selected_option: "",
        })
        if (this.props.exams.question_info) {
            if (this.props.exams.question_info.hasOwnProperty('question')) {
                let question_id = this.props.exams.question_info.question.id;

                let data = {
                    exam_key: this.props.match.params.exam_key,
                    questionId: question_id,
                    selected: this.state.selected_option
                }
                this.props.save_answer(data, this.props.auth.jwt_token)
            }
        }
        //fetch next question from server by using the exam_key
    }

    storeCorrectAnswer = (option) => {
        this.setState({
            correct_option: option.title,
        })
    }

    goHome = () => {
        this.props.history.push('/profile/');
    }

    render() {

        // if is_logged_in === false redirect to login page
        if (!this.props.auth.is_logged_in)
            return <Redirect to="/signin" />;


        let answer_options = [];
        let options_for_jsx = [];

        if (this.props.exams.question_info) {
            if (this.props.exams.question_info.hasOwnProperty('question')) {
                answer_options = this.props.exams.question_info.question.options;
                answer_options = JSON.parse(answer_options);
                options_for_jsx = Array.from(answer_options).map((option, i) => {
                    return (
                        <Radio value={i + 1}>{option.title}</Radio>
                    )
                })
            }
        }



        let question_title = "";
        let sequence = "";
        if (this.props.exams.question_info) {
            if (this.props.exams.question_info.hasOwnProperty('question')) {
                question_title = this.props.exams.question_info.question.title;
            }

            if (this.props.exams.question_info.hasOwnProperty('sequence')) {
                sequence = this.props.exams.question_info.sequence;
            }
        }






        return (
            <div>
                <Row justify="center">
                    <Col>
                        <h1 className="headerText">BYSL Online MCQ Exam</h1>
                    </Col>
                </Row>
                <Row justify="center">
                    <Col md={18} lg={12}>
                        <div className="custom-layout">
                            {
                                this.props.exams.exam_expired == 1 ?
                                    (
                                        <>
                                            <p className="timeExpiredMessage">Time expired</p>
                                            <Button type="primary" onClick={this.goHome}>Go Home To See Result</Button>
                                        </>

                                    )
                                    :
                                    (
                                        <>
                                            <Row justify="space-between">
                                                <Col>
                                                    <h4>Total question: 30</h4>
                                                </Col>
                                                <Col>
                                                    <h4>Total Time: <strong className="timeRemaining">5:00 Min</strong></h4>
                                                    <h4> Passed Time: <strong className="timeRemaining">{this.state.passed_minutes}:{this.state.passed_seconds} Min</strong></h4>
                                                </Col>
                                            </Row>
                                            <div className="questionBox">
                                                {
                                                    this.props.exams.question_loading == 1 ?
                                                        (<div className="questionLoadingSpinner">
                                                            <Spin size="large" />
                                                        </div>) :
                                                        this.props.exams.exam_done == 1 ?
                                                            (
                                                                <div className="examEndedMessageBox">
                                                                    <h1 className="examEndedHeadline">Exam Ended !</h1>
                                                                    <h2>Your achieved marks: {this.props.exams.exam_result.achieved_marks}</h2>
                                                                    <Button type="primary" onClick={this.goHome}>Go Home</Button>
                                                                </div>
                                                            ) :
                                                            (<div>
                                                                <h2> {sequence}. {question_title}</h2>
                                                                {/* <h2> {this.props.exams.question_info.sequence}. {this.props.exams.question_info.question.title}</h2> */}
                                                                <br />
                                                                <Radio.Group onChange={(e) => { this.setState({ selected_option: e.target.value }) }} name="radiogroup">
                                                                    {options_for_jsx}
                                                                </Radio.Group>
                                                                <br />
                                                                <Row justify="end" className="nextQuestion">
                                                                    <Col>
                                                                        <Button type="primary" onClick={this.fetchNextQuestion}>Next</Button>
                                                                    </Col>
                                                                </Row>
                                                            </div>)
                                                }
                                            </div>
                                        </>
                                    )
                            }
                        </div>
                    </Col>
                </Row >
            </div >
        )
    }
}
const mapStateToProps = (state) => ({
    auth: state.auth,
    exams: state.exams,
})

const mapDispatchToProps = dispatch => ({
    fetch_question: (exam_key, jwt_token) => {
        dispatch(fetch_a_question(exam_key, jwt_token))
    },
    exam_expired: (flag) => {
        dispatch(exam_time_expired(flag));
    },
    save_answer: (data, jwt_token) => {
        dispatch(save_answer(data, jwt_token));
    },
    keepExamResult: (result) => {
        dispatch(keep_exam_result(result));
    }
})
export default connect(mapStateToProps, mapDispatchToProps)(Exam)