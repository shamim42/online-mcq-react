import React, { Component } from 'react'
import { Row, Col } from 'antd';
import { Link } from 'react-router-dom';


import { connect } from 'react-redux';
import { make_log_in } from '../../redux/actions/authActions';
import { BASE_URL } from '../../config'
import Axios from 'axios';

class Signup extends Component {
    state = {
        name: "",
        mobile: "",
        password: "",
    }

    handleSignup = (e) => {
        Axios({
            method: "POST",
            url: BASE_URL + "/api/signup/",
            data: {
                name: this.state.name,
                mobile: this.state.mobile,
                password: this.state.password
            }
        }).then(response => {
            if (response.data.status == true) {
                this.props.makeSignedin(response.data.data.name, response.data.data.userId, response.data.data.mobile, response.data.data.jwt)
                this.props.history.push('/profile/');
            }
        })

    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value,
        })
    }
    render() {
        return (
            <div>
                <Row>
                    <Col>
                        <div className='auth-box-container'>
                            <div className="auth-box">
                                <p className="signin-text">BYSL Signup</p>
                                <p>Name</p>
                                <input type="text" name="name" onChange={this.handleChange} className="auth-input-field" />
                                <br />
                                <p>Mobile</p>
                                <input type="text" name="mobile" onChange={this.handleChange} className="auth-input-field" />
                                <br />
                                <p>Password</p>
                                <input type="password" name="password" onChange={this.handleChange} className="auth-input-field" />
                                <br />
                                <button className="auth-input-field signin-button" onClick={this.handleSignup}>SIGNUP</button>
                                <br />
                                <small className="dont-have-an-account-text"><Link to='/signin'>Already have an account ?</Link></small>
                            </div>
                        </div>

                    </Col>
                </Row>
            </div>
        )
    }
}
const mapStateToProps = (state) => ({
    auth: state.auth,
})

const mapDispatchToProps = (dispatch) => ({
    makeSignedin: (name, userId, mobile, jwt_token) => {
        dispatch(make_log_in(name, userId, mobile, jwt_token));
    }
})
export default connect(mapStateToProps, mapDispatchToProps)(Signup);