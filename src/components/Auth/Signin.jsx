import React, { Component } from 'react'
import { Row, Col } from 'antd';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { make_log_in } from '../../redux/actions/authActions';
import { BASE_URL } from '../../config'
import Axios from 'axios';

class Signin extends Component {
    state = {
        name: "",
        mobile: "",
    }

    handleSignin = (e) => {
        console.log(BASE_URL);
        Axios({
            method: "POST",
            url: BASE_URL + "/api/login/",
            data: {
                mobile: this.state.mobile,
                password: this.state.password
            }
        }).then(response => {
            if (response.data.status == true) {
                console.log(response.data)

                this.props.makeSignedin(response.data.data.name, response.data.data.userId, response.data.data.mobile, response.data.data.jwt_token)
                this.props.history.push('/profile/');
            }
        })

    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value,
        })
    }

    render() {
        return (
            <div>
                <Row>
                    <Col>
                        <div className='auth-box-container'>
                            <div className="auth-box">
                                <p className="signin-text">BYSL Signin</p>
                                <p>Mobile</p>
                                <input type="text" onChange={this.handleChange} name="mobile" className="auth-input-field" />
                                <br />
                                <p>Password</p>
                                <input type="password" onChange={this.handleChange} name="password" className="auth-input-field" />
                                <br />
                                <button type="submit" className="auth-input-field signin-button" onClick={this.handleSignin}>SIGN IN</button>
                                <br />
                                <small className="dont-have-an-account-text"><Link to='/signup'>Don't have an account ?</Link></small>
                            </div>

                        </div>

                    </Col>
                </Row>
            </div>
        )
    }
}
const mapStateToProps = (state) => ({
    auth: state.auth,
})

const mapDispatchToProps = (dispatch) => ({
    makeSignedin: (name, userId, mobile, jwt_token) => {
        dispatch(make_log_in(name, userId, mobile, jwt_token));
    }
})
export default connect(mapStateToProps, mapDispatchToProps)(Signin);