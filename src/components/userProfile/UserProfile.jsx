import { Row, Col, Table, Button, Spin, Modal } from 'antd';
import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { change_user_status_in_redux, fetch_all_users, fetch_users_exam_list, make_superuser, remove_superuser } from '../../redux/actions/profileActions';
import { fetch_all_exams } from '../../redux/actions/examActions';

class UserProfile extends Component {
    state = {
        createNewExamModal: false,
    }

    componentDidMount() {
        this.props.fetchUsersExamList(this.props.auth.jwt_token);
        this.props.fetchAllUsers(this.props.auth.jwt_token);
        if (this.props.exams.allExams.length == 0) {
            this.props.fetchAllExamList(this.props.auth.jwt_token);
        }
    }


    createNewExam = () => {
        this.props.history.push('/create-exam')
    }

    editExam = (id) => {
        console.log(id)
    }


    render() {
        // if is_logged_in === false redirect to login page
        if (!this.props.auth.is_logged_in)
            return <Redirect to="/signin" />;


        // const dataSource = [
        //     {
        //         key: '1',
        //         name: 'Mike',
        //         age: 32,
        //         address: '10 Downing Street',
        //     },
        //     {
        //         key: '2',
        //         name: 'John',
        //         age: 42,
        //         address: '10 Downing Street',
        //     },
        //     {
        //         key: '2',
        //         name: 'John',
        //         age: 42,
        //         address: '10 Downing Street',
        //     },
        // ];

        const dataSource = Array.from(this.props.profile.users_exams).map((element, index) => {
            return (
                {
                    "key": index,
                    "title": element.exam_name,
                    "taken_time": element.taken_time,
                    "achieved_marks": element.achieved_marks
                }
            )
        })

        const userTableData = Array.from(this.props.profile.all_users).map((element, index) => {
            return (
                {
                    "key": index,
                    "name": element.name,
                    "mobile": element.mobile,
                    "is_superuser": element.is_superuser,
                    "user_id": element.id
                }
            )
        })

        const allExamTableData = Array.from(this.props.exams.allExams).map((element, index) => {
            return (
                {
                    "key": index,
                    "title": element.title,
                    "total_marks": element.total_marks,
                    "duration": element.duration,
                    "exam_id": element.id
                }
            )
        })



        const columns = [
            {
                title: 'Name',
                dataIndex: 'title',
                key: 'key',
                width: '60%'
            },
            {
                title: 'Taken Time',
                dataIndex: 'taken_time',
                key: 'key',
            },
            {
                title: 'Obtained Marks',
                dataIndex: 'achieved_marks',
                key: 'key',
            },


        ];


        const userTableColumns = [
            {
                title: 'Name',
                dataIndex: 'name',
                key: 'key',
                width: '50%'
            },
            {
                title: 'Mobile',
                dataIndex: 'mobile',
                key: 'key',
                width: '30%'
            },
            {
                title: 'Superuser',
                dataIndex: 'is_superuser',
                key: 'key',
                width: '10%',
                render: (is_superuser) => {
                    if (is_superuser == true) {
                        return <span className="green">Yes</span>
                    }
                    else {
                        return <span className="red">No</span>
                    }
                }
            },
            {
                title: 'Action',
                dataIndex: 'user_id',
                key: 'key',
                width: '10%',
                render: (id, row) => {
                    if (row.is_superuser == true) {
                        return (<Button type="danger" onClick={(row) => this.props.removeSuperuser(id, this.props.auth.jwt_token)} >Remove Superuser</Button>)
                    }
                    else if (row.is_superuser == false) {
                        return (<Button type="primary" onClick={(row) => this.props.makeSuperuser(id, this.props.auth.jwt_token)}>Make Superuser</Button>)
                    }
                },
            },
        ];

        const allExamTableColumns = [
            {
                title: 'Title',
                dataIndex: 'title',
                key: 'key',
                width: '50%'
            },
            {
                title: 'Total Marks',
                dataIndex: 'total_marks',
                key: 'key',
                width: '30%'
            },
            {
                title: 'Duration',
                dataIndex: 'duration',
                key: 'key',
                width: '10%',
            },
            {
                title: 'Action',
                dataIndex: 'exam_id',
                key: 'key',
                width: '10%',
                render: (id, row) => {
                    return (<Button type="danger" onClick={this.editExam.bind(this, id)} >Edit</Button>)
                },
            },
        ];




        return (
            <>
                <Row justify="center">
                    <Col>
                        <h1 className="headerText">BYSL Online MCQ Exam</h1>
                    </Col>
                </Row>
                <Row justify="center">
                    <Col md={18} lg={12}>
                        <div className="custom-layout userProfile">
                            <Row justify="space-between">
                                <Col>
                                    <h3>Hello, {this.props.auth.user_name} !</h3>
                                </Col>
                            </Row>
                            <br />
                            <Row className="niceBox">
                                <Col span={24}>
                                    <Row justify="space-between">
                                        <Col >
                                            <h3><strong>All exams you participated:</strong></h3>
                                        </Col>
                                        <Col>
                                            <Link to="/exam-list">
                                                <Button type="primary">Give Exam</Button>
                                            </Link>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col span={24}>
                                            {

                                                this.props.profile.users_exam_loading == true ?
                                                    (
                                                        <div className="examLoadingSpinner">
                                                            <Spin size="large" />
                                                        </div>
                                                    )
                                                    : (<Table dataSource={dataSource} columns={columns} />)
                                            }
                                        </Col>
                                    </Row>

                                </Col>
                            </Row>

                            <br />
                            <br />
                            <Row className="niceBox">
                                <Col span={24}>
                                    <Row justify="space-between">
                                        <Col >
                                            <h3><strong>All exams in database</strong></h3>
                                        </Col>
                                        <Col>

                                            <Button onClick={this.createNewExam} type="primary">Create New Exam</Button>

                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col span={24}>
                                            {
                                                this.props.profile.users_exam_error ?
                                                    (
                                                        <h3 className="errorMessage">{this.props.profile.users_exam_error}</h3>
                                                    )
                                                    :
                                                    this.props.profile.users_exam_loading == true ?
                                                        (
                                                            <div className="examLoadingSpinner">
                                                                <Spin size="large" />
                                                            </div>
                                                        )
                                                        : (<Table dataSource={allExamTableData} columns={allExamTableColumns} />)
                                            }
                                        </Col>
                                    </Row>

                                </Col>
                            </Row>
                            <br />
                            <br />
                            <Row className="niceBox">
                                <Col span={24}>
                                    <h3><strong>All Users:</strong></h3>
                                    {
                                        this.props.profile.error_in_fetching_all_users ?
                                            (
                                                <h3 className="errorMessage">{this.props.profile.users_exam_error}</h3>
                                            )
                                            :
                                            this.props.profile.users_exam_loading == true ?
                                                (
                                                    <div className="examLoadingSpinner">
                                                        <Spin size="large" />
                                                    </div>
                                                )
                                                : (<Table dataSource={userTableData} columns={userTableColumns} />)
                                    }
                                </Col>
                            </Row>

                        </div>
                    </Col>
                </Row>
            </>
        )
    }
}
const mapStateToProps = (state) => ({
    auth: state.auth,
    profile: state.profile,
    exams: state.exams,
})

const mapDispatchToProps = (dispatch) => ({
    fetchUsersExamList: (jwt_token) => {
        dispatch(fetch_users_exam_list(jwt_token));
    },

    fetchAllExamList: (jwt_token) => {
        dispatch(fetch_all_exams(jwt_token));
    },

    fetchAllUsers: (jwt_token) => {
        dispatch(fetch_all_users(jwt_token));
    },
    makeSuperuser: (proposed_id, jwt_token) => {
        dispatch(make_superuser(proposed_id, jwt_token));
    },
    removeSuperuser: (proposed_id, jwt_token) => {
        dispatch(remove_superuser(proposed_id, jwt_token));
    },
    changeUserStatusInRedux: (user_id, superuser_flag) => {
        dispatch(change_user_status_in_redux(user_id, superuser_flag));
    }
})
export default connect(mapStateToProps, mapDispatchToProps)(UserProfile)