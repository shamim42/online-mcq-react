import React from 'react';
import './App.css';
import {
  BrowserRouter,
  Route,
  Switch,
  Redirect
} from 'react-router-dom';

import Signup from './components/Auth/Signup';
import Signin from './components/Auth/Signin';
import UserProfile from './components/userProfile/UserProfile';
import ExamList from './components/Exam/ExamList';
import Exam from './components/Exam/Exam';
import { Button } from 'antd';
import { connect } from 'react-redux';
import { make_log_out } from './redux/actions/authActions';
import CreateExam from './components/Exam/CreateExam';

function App(props) {
  return (
    <div className="App">
      <div className="signoutButtonHolder">
        {
          props.auth.is_logged_in ?

            (
              <Button type="danger" onClick={props.makeLogout}>Signout</Button>
            )
            :
            ("")
        }
      </div>

      <BrowserRouter>
        <Switch>
          <Route path="/signup" exact component={Signup} />
          <Route path="/signin" exact component={Signin} />
          <Route path="/exam-list" exact component={ExamList} />
          <Route path="/exam/:exam_key" exact component={Exam} />
          <Route path="/profile/" exact component={UserProfile} />
          <Route path="/create-exam/" exact component={CreateExam} />
          <Redirect from="/" to="/profile" />
        </Switch>
      </BrowserRouter>

    </div>
  );
}

const mapStateToProps = (state) => ({
  auth: state.auth,
})

const mapDispatchToProps = (dispatch) => ({
  makeLogout: () => {
    dispatch(make_log_out())
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(App);
